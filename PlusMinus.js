'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function plusMinus(arr) {
    // Write your code here'
    // variabel untuk menampung mana yang +, -< dan 0
    let positive = 0
    let negative = 0
    let zero = 0
    // looping untuk mengambil data satu persatu
    for(let i=0; i<arr.length; i++){
        // membuat kondisi apabila positif
        if(arr[i] > 0){
            positive += 1
        // kondisi apabila negatif
        } else if(arr[i] < 0){
            negative += 1
        // kondisi apabila 0
        } else{
            arr[i] == 0
            zero += 1
        }
    }
    // .tofixed merupakan array method untuk menambahkan angka dibelakang koma
    console.log((positive/arr.length).toFixed(5))
    console.log((negative/arr.length).toFixed(5))
    console.log((zero/arr.length).toFixed(5))

}

function main() {
    const n = parseInt(readLine().trim(), 10);

    const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

    plusMinus(arr);
}
