'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'staircase' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

function staircase(n) {
    // write your code here
    for (let i = 0; i < n; i++){
        let tampung = ''
        for (let j = 1; j < n - i; j++){
            tampung += ' '
        }
        for (let k = 0; k <= i ; k++){
            tampung += '#'
        }
        console.log(tampung);
    }
}

function main() {
    const n = parseInt(readLine().trim(), 10);

    staircase(n);
}
